export const binaryIdByteLength = 12;
export type BinaryId = DataView;
export const EMPTY_ID: BinaryId = new DataView(new ArrayBuffer(binaryIdByteLength));