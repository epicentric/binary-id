import {BinaryId, binaryIdByteLength} from './binary-id';
import {fromByteArray, toByteArray} from 'base64-js';

export function fromBinaryId(id: BinaryId): string
{
    return fromByteArray(new Uint8Array(id.buffer));
}

export function toBinaryId(id: string): BinaryId
{
    const result = new DataView(toByteArray(id).buffer);

    if (result.byteLength !== binaryIdByteLength)
        throw new Error(`String is of invalid length (${result.byteLength} != ${binaryIdByteLength})`);

    return result;
}