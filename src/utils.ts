import {BinaryId, binaryIdByteLength} from './binary-id';
import {dataViewCompare32, dataViewEquals32} from '@epicentric/utilbelt';

export function getNestedBinaryId(id: BinaryId): BinaryId
{
    if (id.byteLength < binaryIdByteLength * 2)
        throw new Error(`BinaryId is too short`);

    return new DataView(id.buffer, 6, 12);
}

export function compareBinaryId(a: BinaryId, b: BinaryId): number
{
    return dataViewCompare32(a, b);
}

export function equalsBinaryId(a: BinaryId|null|undefined, b: BinaryId|null|undefined): boolean
{
    if (a == null || b == null)
        return a == b;

    return dataViewEquals32(a, b);
}

export function isBinaryId(id: any): id is BinaryId
{
    return id instanceof DataView && binaryIdByteLength <= id.byteLength;
}

export function isNestedBinaryId(id: any): id is BinaryId
{
    return id instanceof DataView && id.byteLength === binaryIdByteLength * 2;
}