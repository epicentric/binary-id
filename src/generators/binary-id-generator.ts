import {BinaryId, binaryIdByteLength} from '../binary-id';

// Based on: https://stackoverflow.com/questions/42216219/how-to-set-a-signed-24-bit-integer-in-a-dataview
// Improvements based on comments on the question
function setUint24(dataView: DataView, position: number, value: number): void
{
    dataView.setUint16(position, value >> 8);
    dataView.setUint8(position+2, value);
}

export abstract class BinaryIdGenerator
{
    public counter: number;
    
    public constructor(counter?: number)
    {
        if (undefined === counter)
        {
            const arrayBuffer = new ArrayBuffer(4);
            this.fillRandomValues(new Uint8Array(arrayBuffer, 1, 3));
            this.counter = (new DataView(arrayBuffer)).getUint32(0);
        }
        else
        {
            this.counter = counter;
        }        
    }
    
    public create(): BinaryId
    {
        // 0-5: Unix timestamp milliseconds (big endian, to be sortable)
        // 6-8: counter starting from random offset (big endian, because it's easier to clip)
        // 9-11: random bytes
        const data = new DataView(new ArrayBuffer(binaryIdByteLength));
        // Get the current timestamp
        const now = Date.now();

        // Write the timestamp seconds part
        data.setUint32(0, Math.floor(now / 1000));
        // Write the timestamp milliseconds part
        data.setUint16(4, now % 1000);
        // Write the counter
        setUint24(data, 6, this.counter);

        // Fill indexes 9-10-11 with a random number
        this.fillRandomValues(new Uint8Array(data.buffer, 9));
        
        // Increment counter
        this.counter = (this.counter + 1) % 2**24;

        return data;
    }
    
    public createNested(id: BinaryId): BinaryId
    {
        if (binaryIdByteLength < id.byteLength)
            throw new Error(`Can't nest documentIds multiple times`);

        // 0-5: Unix timestamp milliseconds (big endian, to be sortable)
        // 6-17: Nested BinaryId
        // 18-20: counter starting from random offset (big endian, because it's easier to clip)
        // 21-23: random bytes
        
        // It's ordered like this, so you can performantly search for values that have nested id X between times
        // Y and Z
        
        const data = new DataView(new ArrayBuffer(binaryIdByteLength * 2));
        // Get the current timestamp
        const now = Date.now();

        // Write the timestamp seconds part
        data.setUint32(0, Math.floor(now / 1000));
        // Write the timestamp milliseconds part
        data.setUint16(4, now % 1000);

        // Write the 12 bytes of the nested BinaryId (this is probably the fastest way...)
        data.setUint32(6, id.getUint32(0));
        data.setUint32(10, id.getUint32(4));
        data.setUint32(14, id.getUint32(8));

        // Write the counter to indexes
        setUint24(data, 18, this.counter);

        // Fill indexes 21-22-23 with a random number
        this.fillRandomValues(new Uint8Array(data.buffer, 21));
        
        this.counter = (this.counter + 1) % 2**24;

        return data;
    }
    
    protected abstract fillRandomValues(buffer: Uint8Array): void;
}