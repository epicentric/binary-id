import {BinaryIdGenerator} from './binary-id-generator';
import crypto from 'crypto';

export class NodeIdGenerator extends BinaryIdGenerator
{
    protected fillRandomValues(buffer: Uint8Array): void
    {
        // TODO: optimize this by getting 65536 bytes at once, and using that as a pool 
        crypto.randomFillSync(buffer);
    }
}