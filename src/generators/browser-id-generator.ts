import {BinaryIdGenerator} from './binary-id-generator';

export class BrowserIdGenerator extends BinaryIdGenerator
{
    protected fillRandomValues(buffer: Uint8Array): void
    {
        crypto.getRandomValues(buffer);
    }
}